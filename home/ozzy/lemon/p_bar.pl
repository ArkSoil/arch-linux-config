#!/usr/bin/perl


use POSIX;
use strict;
use warnings;

sub p_bar{#string, pos, max_pos,over/under
	my @string_a = split('', $_[0]);
	my $step = floor(@string_a*$_[2]/$_[1]);

	return "%{U#".$_[4]."}%{+".$_[3]."}",
               (@string_a[0..$step-1]),
               "%{U-}%{-".$_[3]."}",(
               @string_a[$step..@string_a-1]);
}
1;
