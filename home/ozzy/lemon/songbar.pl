#!/usr/bin/perl
#mpd song bar lemon edition 0.02v


use POSIX;
use strict;
use warnings;


#my $ctrl = $ENV{'BLOCK_BUTTON'};
#if   ($ctrl == 1){`mpc toggle`;}
#elsif($ctrl == 4){`mpc seek +5`;}
#elsif($ctrl == 5){`mpc seek -5`;}


#mpc output
my @mpc_out = split('\n', `mpc`);
#status indicators
my $switch = `mpc current`;
my @status = split(' ', $mpc_out[1]); 
#song name
my @name_str = split('', $mpc_out[0]);
#end of pango indicator
my $end = 0;
#precent of time elapsed
my $elapsed = $status[3] ;
my $elapsed =~ s/\D//g;
my $exit_val = $? >> 8;

#time_color.pl
my $color;
my $time = `date +%H` * 3600 + `date +%M` * 60 + `date +%S`;
$time = ceil(255*ceil($time/86400*100)/100);
my $r_time = 255 - $time;
$time = sprintf("%X", $time);
$r_time = sprintf("%X", $r_time);

if(hex $time < 16){
	$time = 0 . $time;
}
if(hex $r_time < 16){
	$r_time = 0 . $r_time;
}

if   (`date +%u` == 1){$color = "#FF" . $time . "00";}
elsif(`date +%u` == 2){$color = "#" . $r_time . "FF00";}
elsif(`date +%u` == 3){$color = "#00FF" . $time;}
elsif(`date +%u` == 4){$color = "#00" . $r_time . "FF";}
elsif(`date +%u` == 5){$color = "#" . $time . "00FF";}
elsif(`date +%u` == 6){$color = "#FF00" . $r_time;}
elsif(`date +%u` == 7){$color = "#FF0000";}


if($exit_val == 1){print("%{F".$color."}MPD OFFLINE%{F-}");}
elsif($switch eq '' ){print("%{F".$color."}MPC STOPPED%{F-}");}
else{
	print("%{U" . $color . "}%{+u}");
	for(my $i = 0; $i < @name_str; $i=$i+1){
		if($i >= ceil(@name_str * ( $elapsed / 100))-1 && $end == 0){
			print($name_str[$i] . "%{U-}%{-u}");
			$end = 1;
		}
		else{
			if($name_str[$i] eq "&"){print("&amp;");}
			else{print($name_str[$i]);}
		}
	}
	print(' ' . $status[2]);
}
