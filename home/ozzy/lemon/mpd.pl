#!/usr/bin/perl

use strict;
use warnings;
use open ':std', ':encoding(UTF-8)';
require "/home/ozzy/lemon/p_bar.pl";
use Audio::MPD;
my $mpd       = Audio::MPD->new;

if($mpd->status->state eq "stop"){
	print "MPD STOPPED";
}
else{
	my $time      = $mpd->current->time;
	my $min       = ($time - ($time % 60))/60;
	my $sec       = sprintf("%02d",$time % 60);
	my $song      = $mpd->current->artist." - ".
	                $mpd->current->title;
	my $elapsed   = $mpd->status->time->sofar;
	my $elapsed_p = $mpd->status->time->percent;
	my $txt_limit = 60;
	my @song_a    = split('',$song); 
	if (@song_a   < 60) { $txt_limit = @song_a-1;}
	#my $offset    = $mpd->status->time->seconds_sofar;
	#   $offset    = $offset % @song_a;
	my $offset    = 0;
	   $song      = join ('',@song_a[$offset..$txt_limit+$offset]); 
	
	print(p_bar($song." ".$elapsed."/".$min.":".$sec,100,$elapsed_p,"u",'FFFF00'));
}

