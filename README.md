Required packages:
 - bspwm + xorg setup (wm)
 - lightdm (de)
 - compton or picom? (compositor)
 - sxhkd (key binding)
 - lemonbar (top bar)
 - vim + python3 support (main editor)
 - feh (wallpapper)
 - rofi (run programs)

